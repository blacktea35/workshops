// input: []({}
// stack: (

function isValid(value) {
    let stack = [];
    let open = ['(', '{', '['];
    let close = [')', '}', ']'];
    for (let i = 0; i < value.length; i++) {
        if (open.includes(value[i])) {
            stack.push(value[i]);
            continue;
        }
        if (close.includes(value[i])) {
            if (stack[stack.length-1] === open[close.indexOf(value[i])]) {
                stack.pop();
            } else {
                return 'NO';
            }
        }
    }
    return stack.length === 0 ? 'YES' : 'NO';
}

function braces(values) {
    return values.map(isValid);
}

console.log(braces(['[]({}', '(((((', '(){([([{}])])}', '{[]()}{{[]()}()}']));

