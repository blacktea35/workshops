function EventEmitter() {
    this.events = {};
    this.subscribe = function (eventName, cb) {
        this.events[eventName] = this.events[eventName] || [];
        this.events[eventName].push(cb);
        return () => {
            let events = this.events[eventName];
            this.events[eventName] = events.filter((event) => {
                return event !== cb;
            });
        }
    };
    this.emit = function (eventName) {
        if (this.events[eventName]) {
            this.events[eventName].forEach(cb => {
                cb.apply(null, Array.prototype.slice.call(arguments, 1))
            })
        }
    };
}

const getData = (timeout) => new Promise((res) => {
    setTimeout(() => {
        res('test')
    }, timeout)
});

const ee = new EventEmitter();

ee.subscribe('test', (data) => {
    console.log('test event', data)
});

const unsubscribe = ee.subscribe('test', (data) => {
    console.log('test event2', data)
});

ee.subscribe('test', (data) => {
    console.log('test event3', data)
});

ee.subscribe('lololo', (data) => {
    console.log('lololo', data)
});

ee.subscribe('pewpew', (err, data) => {
    console.log('pewpew', err, data)
});

getData(2000).then((data) => {
    ee.emit('test', data)
});

ee.emit('pewpew', 'some error', 'puff');

unsubscribe();



