// compose(a, b, c, d) -> Q(z) -> a(b(c(d(z))))

// compose(pow, x2)(n) === pow(x2(n)) /// pow(n)
// compose(pow, x2, sum)(a, b) === pow(x2(sum(a, b)))

function pow(n) {
    return n * n;
}

function x2(n) {
    return n * 2;
}

function sum(a, b) {
    return a + b;
}

function compose(...fns) {
    return function() {
        let result = fns[fns.length-1].apply(null, arguments);
        for (let i = fns.length-2; i >= 0; i--) {
            result = fns[i](result);
        }
        return result;
    }
}

const magic = compose(pow, x2, sum);
console.log(magic(4,2));