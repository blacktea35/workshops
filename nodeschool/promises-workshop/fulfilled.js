'use strict';

let promise = new Promise((res) => {
    setTimeout(() => {
        res('FULFILLED!');
    }, 300);
});

promise.then(console.log);

