const promise = new Promise((res, rej) => {
    setTimeout(() => {
        rej(new Error('REJECTED!'));
    }, 300);
});

function onReject(error) {
    console.log(error.message);
}

promise.then(null, onReject);
