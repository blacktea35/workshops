function parsePromised() {
    return new Promise((res, rej) => {
        try {
            res(JSON.parse(process.argv[2]));
        } catch(err){
            rej(err);
        }
    })
}

parsePromised().then(console.log).then(console.log);
