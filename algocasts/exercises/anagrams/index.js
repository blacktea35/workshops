// --- Directions
// Check to see if two provided strings are anagrams of eachother.
// One string is an anagram of another if it uses the same characters
// in the same quantity. Only consider characters, not spaces
// or punctuation.  Consider capital letters to be the same as lower case
// --- Examples
//   anagrams('rail safety', 'fairy tales') --> True
//   anagrams('RAIL! SAFETY!', 'fairy tales') --> True  railsafety fairytales
//   anagrams('Hi there', 'Bye there') --> False

function anagrams(stringA, stringB) {

    //1)
    // function getCharsObject(string) {
    //     let chars = {};
    //     for (let char of string.replace(/[^\w]/g,'').toLowerCase()) {
    //         chars[char] = chars[char]++ || 1;
    //     }
    //     return chars;
    // }
    //
    // let charsA = getCharsObject(stringA);
    // let charsB = getCharsObject(stringB);
    //
    // if (Object.keys(charsA).length !== Object.keys(charsB).length) {
    //     return false;
    // }
    //
    // return Object.keys(charsA).every(key => {
    //    return charsA[key] === charsB[key];
    // });
    //
    //2)
    let clearedStringA = stringA.replace(/[^\w]/g, '').toLowerCase();
    let clearedStringB = stringB.replace(/[^\w]/g, '').toLowerCase();
    for (let char of clearedStringA) {
        if (clearedStringB.indexOf(char) === -1) {
            return false;
        }
        clearedStringB = clearedStringB.replace(char,'');
    }
    return clearedStringB === '';
    //
    //3)
    // let clearedArrayA = stringA.replace(/[^\w]/g, '').toLowerCase().split('').sort();
    // let clearedArrayB = stringB.replace(/[^\w]/g, '').toLowerCase().split('').sort();
    //
    // return clearedArrayA.every((char, i) => {
    //     return char === clearedArrayB[i];
    // });

    //4)
    // let clearedStringA = stringA.replace(/[^\w]/g, '').toLowerCase().split('').sort().join('');
    // let clearedStringB = stringB.replace(/[^\w]/g, '').toLowerCase().split('').sort().join('');
    // return (clearedStringA === clearedStringB);
}

module.exports = anagrams;
