// --- Directions
// Given an array and chunk size, divide the array into many subarrays
// where each subarray is of length size
// --- Examples
// chunk([1, 2, 3, 4], 2) --> [[ 1, 2], [3, 4]]
// chunk([1, 2, 3, 4, 5], 2) --> [[ 1, 2], [3, 4], [5]]
// chunk([1, 2, 3, 4, 5, 6, 7, 8], 3) --> [[ 1, 2, 3], [4, 5, 6], [7, 8]]
// chunk([1, 2, 3, 4, 5], 4) --> [[ 1, 2, 3, 4], [5]]
// chunk([1, 2, 3, 4, 5], 10) --> [[ 1, 2, 3, 4, 5]]

function chunk(array, size) {
    //1)
    // let result = [];
    // for (let i = 0; i < array.length; i = i + size) {
    //     result.push(array.slice(i, i + size));
    // }
    // return result;
    //
    //2)
    let chunked = [];
    for (let elem of array) {
        let last = chunked[chunked.length - 1];

        if (!last || last.length === size) {
            chunked.push([elem]);
        } else {
            last.push(elem);
        }
    }

    return chunked;
}

module.exports = chunk;
