// --- Directions
// Implement classes Node and Linked Lists
// See 'directions' document

class Node {
    constructor (data, next = null) {
        this.data = data;
        this.next = next;
    }
}

class LinkedList {
    constructor() {
        this.head = null;
    }

    insertFirst(data) {
        this.head = new Node(data, this.head);
    }

    size() {
        let result = 0;
        let node = this.head;

        while (node) {
            result++;
            node = node.next;
        }

        return result;
    }

    getFirst() {
        return this.head;
    }

    getLast() {
        // let node = this.head;
        // if (node) {
        //     while (node.next) {
        //         node = node.next;
        //     }
        // }
        // return node;
        return this.getAt(this.size() - 1);
    }

    clear() {
        this.head = null;
    }

    removeFirst() {
        if (this.head) {
            this.head = this.head.next;
        }
    }

    removeLast() {
        let node = this.head;
        if (node && !node.next) {
            this.head = null;
        }
        if (node && node.next) {
            let next = node.next;
            while (next.next) {
                node = next;
                next = next.next;
            }
            node.next = null;
        }
    }

    insertLast(data) {
        let node = this.getLast();
        if (node) {
            node.next = new Node(data);
        } else {
            this.head = new Node(data);
        }
    }

    getAt(index) {
        let node = this.head;
        if (node && index > 0) {
            for (let i = 1; i <= index; i++) {
                if (node.next) {
                    node = node.next;
                } else {
                    return null;
                }
            }
        }
        return node;
    }

    // empty
    // [head]
    // [1]
    //  node = 1, index = 3
    //
    // default: [head] -> []

    removeAt(index) {
        if (index > (this.size() - 1)) {
            return;
        }
        if (index === 0) {
            this.removeFirst();
            return;
        }
        let previous = this.getAt(index - 1);
        if (this.getAt(index).next) {
            previous.next = this.getAt(index).next;
        } else {
            previous.next = null;
        }
    }

    insertAt(data, index) {
        if (index >= (this.size() - 1)) {
            this.insertLast(data);
            return;
        }
        if (index === 0) {
            this.insertFirst(data);
            return;
        }
        this.getAt(index - 1).next = new Node(data, this.getAt(index));
    }

    forEach(fn) {
        let node = this.head;
        let counter = 0;
        while(node) {
            fn(node, counter);
            node = node.next;
            counter ++;
        }
    }

    *[Symbol.iterator]() {
        let node = this.head;
        while (node) {
            yield node;
            node = node.next;
        }
    }

}

module.exports = { Node, LinkedList };
