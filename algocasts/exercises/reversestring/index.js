// --- Directions
// Given a string, return a new string with the reversed
// order of characters
// --- Examples
//   reverse('apple') === 'leppa'
//   reverse('hello') === 'olleh'
//   reverse('Greetings!') === '!sgniteerG'

function reverse(str) {
    //1)
    //return str.split('').reverse().join('');
    //
    //2)
    // str = str.split('');
    // const length = str.length;
    // for (let i = 0; i < length/2; i++) {
    //     [str[length - i - 1], str[i]] = [str[i], str[length - i - 1]]
    //     // let curr = str[i];
    //     // str[i] = str[length - i - 1];
    //     // str[length - i - 1] = curr;
    // }
    // return str.join('');
    //
    //3)
    // let reversed = '';
    // for (let char of str) {
    //     reversed = char + reversed;
    // }
    // return reversed;
    //
    //4)
    return str.split('').reduce((acc, char) => char + acc, '');
}

module.exports = reverse;
