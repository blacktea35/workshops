// --- Directions
// Implement bubbleSort, selectionSort, and mergeSort

function bubbleSort(arr) {
    for (let i = 0; i < arr.length; i++) {
        for (let j = 0; j <= arr.length - i - 1; j++) {
            if (arr[j] > arr[j+1]) {
                [arr[j], arr[j+1]] = [arr[j+1], arr[j]];
            }
        }
    }
    return arr;
}

function selectionSort(arr) {
    for (let i = 0; i < arr.length; i++) {
        let minInd = i;
        for (let j = i + 1; j < arr.length; j++) {
            if (arr[minInd] > arr[j]) {
                minInd  = j;
            }
        }
        if (minInd !== i) {
            [arr[i], arr[minInd]] = [arr[minInd], arr[i]];
        }
    }
    return arr;
}

function mergeSort(arr) {
    if (arr.length === 1) {
        return arr;
    }

    let left = arr.slice(0, arr.length/2);
    let right = arr.slice(arr.length/2);

    return merge(mergeSort(left), mergeSort(right));
}

function merge(left, right) {
    let results = [];
    while (left.length && right.length) {
        results.push(left[0] > right[0] ? right.shift() : left.shift());
    }
    return [...results, ...left, ...right];
}

module.exports = { bubbleSort, selectionSort, merge, mergeSort };
