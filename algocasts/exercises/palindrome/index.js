// --- Directions
// Given a string, return true if the string is a palindrome
// or false if it is not.  Palindromes are strings that
// form the same word if it is reversed. *Do* include spaces
// and punctuation in determining if the string is a palindrome.
// --- Examples:
//   palindrome("abba") === true
//   palindrome("abcdefg") === false

function palindrome(str) {
    //1)
    // const length = str.length;
    // const reversed = str.substring(Math.ceil(length/2)).split('').reverse().join('');
    // return str.substring(0, length/2) === reversed;
    //
    //2)
    //return str === str.split('').reduce((acc, char) => char + acc, '');
    //
    //3)
    return str.split('').every((char, i) => char === str[str.length - i - 1]);
}

module.exports = palindrome;
