// --- Directions
// Given the root node of a tree, return
// an array where each element is the width
// of the tree at each level.
// --- Example
// Given:
//     0
//   / |  \
// 1   2   3
// |       |
// 4       5
// Answer: [1, 3, 2]

function levelWidth(root) {
    let countArr = [0];
    let nodeArr = [root, '!!!'];
    //let counter = 0;

    while (nodeArr.length > 1) {
        const node = nodeArr.shift();
        if (node === '!!!') {
            nodeArr.push('!!!');
            countArr.push(0);
            //counter++;
        } else {
            // if (countArr[counter]) {
            //     countArr[counter]++;
            // } else {
            //     countArr[counter] = 1;
            // }
            countArr[countArr.length - 1]++;
            nodeArr.push(...node.children);
        }
    }
    return countArr;
}

module.exports = levelWidth;
