// --- Directions
// Write a function that accepts a positive number N.
// The function should console log a step shape
// with N levels using the # character.  Make sure the
// step has spaces on the right hand side!
// --- Examples
//   steps(2)
//       '# '
//       '##'
//   steps(3)
//       '#  '
//       '## '
//       '###'
//   steps(4)
//       '#   '
//       '##  '
//       '### '
//       '####'

function steps(n, row = 0, stair = '') {
    //1)
    // for (let i = 1; i <= n; i++) {
    //     console.log('#'.repeat(i) + ' '.repeat(n - i));
    // }
    //
    //2)
    // for (let i = 1; i <= n; i++) {
    //     let stair = '';
    //     for (let j = 1; j <= n; j++) {
    //         stair += j > i ? ' ' : '#';
    //     }
    //     console.log(stair);
    // }
    //
    //3)
    if (row === n) {
        return;
    }
    stair += (stair.length <= row) ? '#' : ' ';
    if (stair.length === n) {
        console.log(stair);
        return steps(n, row + 1);
    }
    steps(n, row, stair);
}

module.exports = steps;
