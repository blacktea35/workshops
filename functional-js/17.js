function curryN(fn, n) {
    var arity = n || fn.length;
    return function(a) {
        return function(b) {
            return function(c) {
                return a + b + c;
                fn();
            }
        }
    }
}

module.exports = curryN;

