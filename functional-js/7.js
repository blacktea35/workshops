function reduce(arr, fn, initial) {
    var i = arr.length;
    var result = initial;
    if (i === 0) {
        return result;
    } else {
        result = fn(result, arr[i-1], i, arr);
        arr = arr.slice(0, i-1);
        return reduce(arr, fn, result);
    }
}

module.exports = reduce;
