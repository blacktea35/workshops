var slice = Array.prototype.slice;

function logger(namespace) {
    var logLevel = slice.call(arguments, 0);
    return function() {
        console.log.apply(this, logLevel.concat(slice.call(arguments)));
    }
}

module.exports = logger;