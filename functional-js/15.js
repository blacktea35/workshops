function loadUsers(userIds, load, done) {
    var users = [];
    userIds.forEach(id, function(){
        load(id, function(user){
            if (user) {
                users.push(user)
            }
        })
    });
}

module.exports = loadUsers;