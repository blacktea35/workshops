function getDependencies(tree, result) {
    var dependencies = tree && tree.dependencies || [];
    result = result || [];
    Object.entries(dependencies).forEach(function(dependency){
        if (dependency[1].hasOwnProperty('version')) {
            var resultEntity = dependency[0].concat('@', dependency[1].version);
            if (!result.includes(resultEntity)) {
                result.push(resultEntity);
            }
        }
        getDependencies(dependency[1], result);
    });
    return result.sort();
}

module.exports = getDependencies;