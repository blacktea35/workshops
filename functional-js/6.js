function countWords(inputWords) {
    var obj = {};
    return inputWords.reduce(function(acc, currentWord) {
        acc[currentWord] = acc[currentWord] ? ++acc[currentWord] : 1;
        return acc;
    }, obj)
}

module.exports = countWords;
