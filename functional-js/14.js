function repeat(operation, num) {
    // Modify this so it doesn't cause a stack overflow!
    if (num <= 0) return {done: true};
    operation();

    var count = num - 1;
    return {
        done: count <= 0,
        next: function() {
            if (!this.done && count > 0) {
                count--;
                operation()
            } else {
                this.done = true
            }
        }
    }
}

function trampoline(fn) {
    var what = fn();

    while (!what.done) {
        what.next()
    }
}

module.exports = function(operation, num) {
    // You probably want to call your trampoline here!
    trampoline(function() {
        return repeat(operation, num)
    })
};