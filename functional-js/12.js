function Spy(target, method) {
    var prevFunctional = target[method];

    var counter = {
        count: 0
    };

    target[method] = function() {
        counter.count++;
        return prevFunctional.apply(this, arguments);
    };

    return counter;
}

module.exports = Spy;
