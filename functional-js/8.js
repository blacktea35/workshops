function duckCount() {
    return Array.prototype.slice.call(arguments).filter(function(param) {
        return Object.prototype.hasOwnProperty.call(param, 'quack')
    }).length
}

module.exports = duckCount;
