module.exports = function arrayMap(arr, fn) {
    return arr.reduce(function(acc, curr) {
        Array.prototype.push.call(acc, fn(curr));
        return acc;
    }, []);
};
